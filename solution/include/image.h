#ifndef IMAGE_H
#define IMAGE_H
#include "pixel.h"
#include <stdint.h>
struct image {
  uint64_t width, height;
  struct pixel* data;
};
struct pixel getPixel(struct image const* img, uint64_t x, uint64_t y);
void setPixel(struct image* img, uint64_t x, uint64_t y, struct pixel p);
#endif
