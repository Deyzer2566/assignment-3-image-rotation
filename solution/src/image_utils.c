#include "image_utils.h"
#include <stdlib.h>
struct maybe_image createImage(uint64_t width, uint64_t height){
        struct image img={.width=width, .height=height, .data=malloc(sizeof(struct pixel)*width*height)};
        if(img.data == NULL)
                return bad_image;
        return good_image(img);
}
void deleteImage(struct image* img){
        free(img->data);
}
