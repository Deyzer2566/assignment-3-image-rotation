#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"
#include "image_utils.h"
struct maybe_image rotate0(struct image const source);
struct maybe_image rotate90(struct image const source);
struct maybe_image rotate180(struct image const source);
struct maybe_image rotate270(struct image const source);
#endif
