#ifndef BMP_H
#define BMP_H
#include "image.h"
#include  <stdint.h>
#include  <stdio.h>

/*
 * К сожалению, код структуры, представленный в инструкции к работе,
 * некорректен, т.к. судя по названию полей, это структура 
 * bmp_header + bmp_info версии 3.
 * Главная ошибка: biHeight имеет тип int32_t, т.к. знак указывает
 * на порядок рядов данных в памяти
 */
struct __attribute__((__packed__)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        int32_t biWidth;
        int32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        int32_t biXPelsPerMeter;
       	int32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  /* коды других ошибок  */
  READ_TOO_OLD,
  READ_ERROR_DURING_READING,
  READ_CANT_ALLOC_MEMORY
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );
#endif
