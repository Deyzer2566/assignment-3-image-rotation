#include "rotate.h"
#include "image_utils.h"
struct maybe_image rotate(struct image const source, uint64_t newWidth, uint64_t newHeight, void (*transform) (struct image*, uint64_t, uint64_t, struct pixel)){
	struct maybe_image newImage = createImage(newWidth, newHeight);
	if(!newImage.isCorrect)
		return newImage;
	for(uint64_t x = 0;x<source.width;x++)
                for(uint64_t y = 0;y<source.height;y++){
			transform(&newImage.img, x, y, getPixel(&source, x, y));
                }
        return newImage;
}
void rotate0Transform (struct image* img, uint64_t x, uint64_t y, struct pixel p){
        setPixel(img, x, y, p);
}
struct maybe_image rotate0(struct image const source){
        return rotate(source, source.width, source.height, rotate0Transform);
}
void rotate90Transform (struct image* img, uint64_t x, uint64_t y, struct pixel p){
	setPixel(img, img->width-1-y, x, p);
}
struct maybe_image rotate90(struct image const source){
	return rotate(source, source.height, source.width, rotate90Transform);
}
void rotate180Transform (struct image* img, uint64_t x, uint64_t y, struct pixel p){
	setPixel(img, img->width-1-x, img->height-1-y, p);
}
struct maybe_image rotate180(struct image const source){
        return rotate(source, source.width, source.height, rotate180Transform);
}
void rotate270Transform (struct image* img, uint64_t x, uint64_t y, struct pixel p){
        setPixel(img, y, img->height-1-x, p);
}
struct maybe_image rotate270(struct image const source){
        return rotate(source, source.height, source.width, rotate270Transform);
}
