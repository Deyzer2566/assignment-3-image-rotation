#include "bmp.h"
#include "image.h"
#include "image_utils.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>
#define ARGC_COUNT 4
#define ERROR_CODE (-1)
enum ARGS_INDEXES{SOURCE_FILE_INDEX=1,DESTINATION_FILE_INDEX=2,ANGLE_INDEX=3};

int main( int argc, char** argv ) {
    if(argc < ARGC_COUNT){
	    fprintf(stderr, "Недостаточно параметров!\nФормат команды: image-transformer <source-image> <transformed-image> <angle>\n");
	    return ERROR_CODE;
    }
    FILE* file = fopen(argv[SOURCE_FILE_INDEX], "rb");
    if(file == NULL){
	    fprintf(stderr, "Файл не найден!\n");
            return ERROR_CODE;
    }
    struct image img;
    enum read_status in_status = from_bmp(file, &img);
    fclose(file);
    switch (in_status){
	case READ_INVALID_SIGNATURE:
                fprintf(stderr, "Ошибка в сигнатуре файла!\n");
                return ERROR_CODE;
	case READ_INVALID_HEADER:
		fprintf(stderr, "Ошибка в BMP заголовке файла!\n");
		return ERROR_CODE;
	case READ_INVALID_BITS:
		fprintf(stderr, "Количество бит на пиксель не поддерживается!\n");
                return ERROR_CODE;
	case READ_TOO_OLD:
                fprintf(stderr, "Версия BMP заголовка не поддерживается!\n");
                return ERROR_CODE;
	case READ_ERROR_DURING_READING:
                fprintf(stderr, "Ошибка во время чтения файла!\n");
                return ERROR_CODE;
	case READ_CANT_ALLOC_MEMORY:
		fprintf(stderr, "Не удалось выделить память!\n");
		return ERROR_CODE;
	case READ_OK:
		break;
    }
    struct maybe_image rotated;
    int angle = atoi(argv[ANGLE_INDEX]);
    switch(angle){
	    case 0:
		    rotated=rotate0(img);
		    break;
	    case 90:case -270:
		    rotated=rotate90(img);
		    break;
	    case 180: case -180:
		    rotated=rotate180(img);
		    break;
	    case 270: case -90:
		    rotated=rotate270(img);
		    break;
	    default:
		    fprintf(stderr, "Неверное значение угла поворота!\n");
		    deleteImage(&img);
		    return ERROR_CODE;
    }
    deleteImage(&img);
    if(!rotated.isCorrect){
	    fprintf(stderr, "Выделить память для новой картинки");
	    return ERROR_CODE;
    }
    FILE* fout = fopen(argv[DESTINATION_FILE_INDEX], "wb");
    if(fout == NULL){
	fprintf(stderr, "Не удалось открыть/создать выходной файл!\n");
	deleteImage(&rotated.img);
        return ERROR_CODE;
    }
    enum write_status out_status = to_bmp(fout, &rotated.img);
    fclose(fout);
    switch (out_status){
	    case WRITE_ERROR:
		fprintf(stderr, "Не удалось сохранить изображение!\n");
	        deleteImage(&rotated.img);
	        return ERROR_CODE;
	    case WRITE_OK:
		break;
    }
    deleteImage(&rotated.img);
    return 0;
}
