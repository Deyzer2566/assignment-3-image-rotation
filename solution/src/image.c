#include "image.h"
struct pixel getPixel(struct image const* img, uint64_t x, uint64_t y){
	return img->data[y*img->width+x];
}
void setPixel(struct image* img, uint64_t x, uint64_t y, struct pixel p){
	img->data[y*img->width+x]=p;
}
