#ifndef IMAGE_UTILS_H
#define IMAGE_UTILS_H
#include "image.h"
#include <stdbool.h>
struct maybe_image{
	struct image img;
	bool isCorrect;
};
#define bad_image ((struct maybe_image){.isCorrect=false})
#define good_image(im) ((struct maybe_image){.isCorrect=true, .img=(im)})
struct maybe_image createImage(uint64_t width, uint64_t height);
void deleteImage(struct image* img);
#endif
