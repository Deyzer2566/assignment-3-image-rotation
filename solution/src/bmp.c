#include "bmp.h"
#include "image.h"
#include "image_utils.h"
#include "pixel.h"
#include <stdio.h>
#define BMP_HEADER1 0x4D42
#define BMP_HEADER2 0x424D
#define BMP_BITS_PER_PIXEL 24
#define BMP_RESERVED 0
#define PADDING 4
#define BMP_HEADER_V3_SIZE 40
#define BMP_COMPRESSION 0
#define BMP_PLANES 1
static long get_padding(uint64_t width){
	return (long)((PADDING-(width*sizeof(struct pixel))%PADDING)%PADDING);
}
enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header;
	fread(&header, sizeof(struct bmp_header), 1, in);
	if(header.bfType != BMP_HEADER1 && header.bfType != BMP_HEADER2)
		return READ_INVALID_SIGNATURE;
	if(header.bfReserved != BMP_RESERVED)
		return READ_INVALID_HEADER;
	if(header.biBitCount != BMP_BITS_PER_PIXEL)
		return READ_INVALID_BITS;
	if(header.biSize < BMP_HEADER_V3_SIZE)
		return READ_TOO_OLD;
	struct maybe_image alloced_image = createImage((uint64_t)header.biWidth, (uint64_t)header.biHeight);
	if(!alloced_image.isCorrect)
		return READ_CANT_ALLOC_MEMORY;
	*img = alloced_image.img;
	fseek(in, header.bOffBits, SEEK_SET);
	long padding = get_padding(img->width);
	for(size_t i = 0;i<img->height;i++){
		if(fread(img->data+(img->height>0?((img->height-1)*img->width-img->width*i):(img->width*i)), sizeof(struct pixel), img->width, in) != img->width){
			return READ_ERROR_DURING_READING;
		}
		fseek(in, padding, SEEK_CUR);
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img){
	struct bmp_header header={0};
	header.bfType=BMP_HEADER1;
	long padding = get_padding(img->width);
	header.bfileSize=sizeof(struct bmp_header) + (sizeof(struct pixel)*img->width+padding)*img->height;
	header.bfReserved=BMP_RESERVED;
	header.bOffBits=sizeof(struct bmp_header);
	header.biSize = BMP_HEADER_V3_SIZE;
	header.biWidth=(int32_t)img->width;
	header.biHeight=(int32_t)img->height;
	header.biPlanes=BMP_PLANES;
	header.biBitCount=BMP_BITS_PER_PIXEL;
	header.biCompression=BMP_COMPRESSION;
	header.biSizeImage=(sizeof(struct pixel)*img->width+padding)*img->height;
	if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
		return WRITE_ERROR;
	uint8_t paddingBytes[] = {0,0,0};
	for(uint64_t row = 0; row < img->height; row++){
		if(fwrite(img->data+(img->height-1-row)*img->width, sizeof(struct pixel), img->width, out) != img->width)
			return WRITE_ERROR;
		if(fwrite(paddingBytes, 1, padding, out) != padding)
			return WRITE_ERROR;
	}
	return WRITE_OK;
}
